# Requirements
  - Java 9 or higher.
  - Maven 3.6 
  - Obtain API Key from https://darksky.net/ and set it when running the application

# Swagger schema
There is an API definition under `weather-service/src/main/resources/swagger.yaml` which can be used to communicate with our application. 

You can use the [Swagger Editor](http://editor.swagger.io/) to visualize it and generate your client or generate the CURL command, optionally you can use Postman instead.

#  Dark Sky API Key
Assuming you have already created the account, you have two options to set the key into our application
## Via application.properties
Replace `YOUR_SECRET_KEY` with your own key into the following file `weather-service/src/main/resources/application.properties`

## Via command line
Append the following jvmArgument when booting the application `-Dspring-boot.run.jvmArguments="-Dforecast.api.key=YOUR_SECRET_KEY"`
For example
`./mvnw clean spring-boot:run -Dspring-boot.run.jvmArguments="-Dforecast.api.key=MY_AWESOME_KEY"`

# Running the application
 `./mvnw clean spring-boot:run` 