package com.example.weatherservice.models.service;

import com.example.weatherservice.api.mappers.DailyMapper;
import com.example.weatherservice.common.DarkSkyService;
import com.example.weatherservice.models.DailyData;
import com.example.weatherservice.models.Location;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.plogitech.darksky.forecast.ForecastException;
import tk.plogitech.darksky.forecast.GeoCoordinates;
import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.Latitude;
import tk.plogitech.darksky.forecast.model.Longitude;

/**
 * The Weather service
 *
 * @author Dorin Brage
 */
@Service
public class WeatherService {

    private Set<Location> defaultLocations;

    @Autowired
    private DarkSkyService darkSkyService;

    @Autowired
    private DailyMapper mapper;

    @PostConstruct
    private void init() throws Exception {
        defaultLocations = new HashSet<>();
        defaultLocations.add(new Location("Jastarnia", 54.6957, 18.6788));
        defaultLocations.add(new Location("Wladyslawowo", 54.7907, 18.4030));
    }

    /**
     * Returns a list of locations with their weather conditions
     *
     * @param parsedDate the given date in <code>yyyy-MM-DD</code> format
     * @return a list of locations with their weather conditions
     * @throws ForecastException when calling the API
     */
    public List<Location> getWeatherContidionsForLocations(Date parsedDate) throws ForecastException {
        List<Location> locations = new ArrayList<>();

        for (Location location : defaultLocations) {

            GeoCoordinates geoCoordinates = new GeoCoordinates(new Longitude(location.getGeolocation().getLongitude()), new Latitude(location.getGeolocation().getLatitude()));

            // request forecast
            Forecast result = darkSkyService.forecast(geoCoordinates, parsedDate);

            // when searching by Date, only one DailyDataPoint is provided
            location.setDaily(mapper.map(result.getDaily().getData().get(0)));
            locations.add(location);
        }

        return locations;
    }

    /**
     * Returns the suitable location given the date
     *
     * @param parsedDate
     * @return a Location object containing data with the suitable location
     * @throws ForecastException if requesting the forecast fails
     */
    public Location getSuitableLocation(Date parsedDate) throws ForecastException {

        List<Location> locations = getWeatherContidionsForLocations(parsedDate);

        // list with suitable locations
        Map<String, Double> results = new HashMap<>();

        // should be 1 item in the list
        locations.forEach((location) -> {
            DailyData daily = location.getDaily();
            // check if resulted forecast is desired
            if (daily.getWindSpeed() >= 5 && daily.getWindSpeed() <= 18 && daily.getTemperatureLow() >= 5 && daily.getTemperatureHigh() <= 35) {
                // ideal forecast
                double bestLocationFormula = daily.getWindSpeed() * 3 + (daily.getTemperatureLow() + daily.getTemperatureHigh()) / 2;
                results.put(location.getName(), bestLocationFormula);
            }
        });

        // is there are no results return null
        if (results.isEmpty()) {
            return null;
        }

        double highestValue = 0;
        String nameLocation = null;
        Location suitableLocation = null;

        // find highest value
        for (Map.Entry<String, Double> result : results.entrySet()) {
            if (result.getValue() > highestValue) {
                highestValue = result.getValue();
                nameLocation = result.getKey();
            }
        }

        for (Location location : locations) {
            if (location.getName().equals(nameLocation)) {
                suitableLocation = location;
            }
        }

        return suitableLocation;
    }

}
