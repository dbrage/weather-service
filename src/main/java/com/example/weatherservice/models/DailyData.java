package com.example.weatherservice.models;

import java.time.Instant;
import lombok.Data;

/**
 * Daily Data
 */
@Data
public class DailyData {

    private Instant time;
    private String summary;
    private String icon;
    private Instant sunriseTime;
    private Instant sunsetTime;
    private Double moonPhase;
    private Double precipIntensity;
    private Double precipIntensityMax;
    private Double precipProbability;
    private Instant precipIntensityMaxTime;
    private Double precipAccumulation;
    private String precipType;
    private Double temperatureHigh;
    private Instant temperatureHighTime;
    private Double temperatureLow;
    private Instant temperatureLowTime;
    private Double apparentTemperatureHigh;
    private Instant apparentTemperatureHighTime;
    private Double apparentTemperatureLow;
    private Instant apparentTemperatureLowTime;
    private Double temperatureMin;
    private Instant temperatureMinTime;
    private Double temperatureMax;
    private Instant temperatureMaxTime;
    private Double apparentTemperatureMin;
    private Instant apparentTemperatureMinTime;
    private Double apparentTemperatureMax;
    private Instant apparentTemperatureMaxTime;
    private Double dewPoint;
    private Double humidity;
    private Double pressure;
    private Double windSpeed;
    private Double windGust;
    private Instant windGustTime;
    private Integer windBearing;
    private Double cloudCover;
    private Integer uvIndex;
    private Instant uvIndexTime;
    private Double visibility;
    private Double ozone;

}
