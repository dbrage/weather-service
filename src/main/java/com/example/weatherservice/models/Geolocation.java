package com.example.weatherservice.models;

import lombok.Data;

/**
 *
 * @author Dorin Brage
 */
@Data
public class Geolocation {

    private double latitude;
    private double longitude;
}
