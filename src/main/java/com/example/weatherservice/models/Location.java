package com.example.weatherservice.models;

import lombok.Data;

/**
 *
 * @author Dorin Brage
 */
@Data
public class Location {

    private String name;

    private Geolocation geolocation;

    private DailyData daily;

    public Location() {
        // empty constructor
    }

    public Location(String name, double latitude, double longitude) {
        this.name = name;
        this.geolocation = new Geolocation();
        this.geolocation.setLatitude(latitude);
        this.geolocation.setLongitude(longitude);
    }

}
