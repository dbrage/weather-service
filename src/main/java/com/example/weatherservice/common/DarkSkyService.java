package com.example.weatherservice.common;

import java.util.Date;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.APIKey;
import tk.plogitech.darksky.forecast.ForecastException;
import tk.plogitech.darksky.forecast.ForecastRequest;
import tk.plogitech.darksky.forecast.ForecastRequestBuilder;
import tk.plogitech.darksky.forecast.GeoCoordinates;
import tk.plogitech.darksky.forecast.model.Forecast;

/**
 * The Dark Sky service provides the weather condition
 *
 * @author Dorin Brage
 */
@Service
public class DarkSkyService {

    @Value("${forecast.api.key}")
    private String forecastApiKey;

    private final DarkSkyJacksonClient client = new DarkSkyJacksonClient();

    @PostConstruct
    private void init() throws Exception {
        if (forecastApiKey == null) {
            throw new Exception("API Key is missing");
        }
    }

    /**
     * Returns the weather condition given the geoCoordinates and a date.
     *
     * @param geoCoordinates latitude and longitude
     * @param date the given date in <code>yyyy-MM-DD</code> format
     * @return the weather condition given the geoCoordinates and a date.
     * @throws ForecastException when calling the API
     */
    public Forecast forecast(GeoCoordinates geoCoordinates, Date date) throws ForecastException {

        ForecastRequest request = new ForecastRequestBuilder()
                .key(new APIKey(forecastApiKey))
                .units(ForecastRequestBuilder.Units.si)
                .language(ForecastRequestBuilder.Language.en)
                .location(geoCoordinates)
                .time(date.toInstant())
                .build();

        return client.forecast(request);
    }

}
