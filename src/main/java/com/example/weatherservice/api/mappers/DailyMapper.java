package com.example.weatherservice.api.mappers;

import com.example.weatherservice.models.DailyData;
import org.mapstruct.Mapper;
import tk.plogitech.darksky.forecast.model.DailyDataPoint;

/**
 * Mapping from <code>darksky.​forecast.​model</code> to our internal models
 *
 * @author Dorin Brage
 */
@Mapper(componentModel = "spring")
public interface DailyMapper {

    DailyData map(DailyDataPoint data);
}
