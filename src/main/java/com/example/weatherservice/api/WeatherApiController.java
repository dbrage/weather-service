package com.example.weatherservice.api;

import com.example.weatherservice.models.service.WeatherService;
import com.example.weatherservice.models.Location;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.plogitech.darksky.forecast.ForecastException;

/**
 * Weather Api Controller
 *
 * @author Dorin Brage
 */
@RestController
public class WeatherApiController {

    @Autowired
    private WeatherService commonCtrl;

    /**
     * Get Weather conditions given a date
     *
     * @param date the given date in <code>yyyy-MM-DD</code> format
     * @return the weather conditions given a date
     * @throws ForecastException
     */
    @RequestMapping(value = "/weather", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getWeather(@RequestParam String date) throws ForecastException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date parsedDate = null;
        try {
            parsedDate = sdf.parse(date);
        } catch (ParseException ex) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        Location location = commonCtrl.getSuitableLocation(parsedDate);

        if (location == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity(location, HttpStatus.OK);
    }
}
